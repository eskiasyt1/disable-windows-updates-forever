import psutil
import time

def stop_process_by_name(process_name):
    """ Stop a process by its name """
    for proc in psutil.process_iter(['pid', 'name']):
        if proc.info['name'] == process_name:
            try:
                proc.terminate()
                print(f"Terminated process {process_name} with PID {proc.pid}")
            except psutil.NoSuchProcess:
                pass

def monitor_and_stop_process(process_name):
    """ Monitor the process and stop it if it starts """
    while True:
        # Check if the process is running
        found = False
        for proc in psutil.process_iter(['pid', 'name']):
            if proc.info['name'] == process_name:
                found = True
                break
        
        if found:
            # If process is found running, stop it
            stop_process_by_name(process_name)
            # Optional: Add additional actions if needed after stopping the process
        else:
            print(f"{process_name} is not running")

        # Sleep for a few seconds before checking again
        time.sleep(5)

# Replace 'process_name.exe' with the actual process name you want to monitor and stop
if __name__ == "__main__":
    process_name = 'process_name.exe'  # Replace with your process name
    monitor_and_stop_process(process_name)
